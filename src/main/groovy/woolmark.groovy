import javax.imageio.ImageIO
import javax.swing.*
import java.awt.*
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import java.awt.image.BufferedImage

trait Area {
    int x
    int y
    int width
    int height

    def String toString() {
        return "(" + x + "," + y + "," + width + "," + height + ")"
    }
}

class SimpleArea implements Area {

    SimpleArea(int x = 0, int y = 0, int width = 0, int height = 0) {
        this.x = x
        this.y = y
        this.width = width
        this.height = height
    }

}

class Sheep implements Area {

    def MOVE_DELTA_X = 5;

    def MOVE_DELTA_Y = 3;

    def JUMP_FRAME = 4;

    def stretch = false

    private def jumpX  = 0

    private def jumpCount = Integer.MIN_VALUE

    Sheep(Area size, Area frame, Area ground, Area fence) {

        x = ground.width + size.width

        y = ground.y + Random.newInstance().nextInt(ground.height - size.height)

        width = size.width

        height = size.height

        jumpX = -1 * (y - frame.height) * fence.width / fence.height + (frame.width - fence.width) / 2

    }

    def run() {

        x -= MOVE_DELTA_X

        if (jumpCount < 0 && jumpX < x && x <= jumpX + MOVE_DELTA_X) {
            jumpCount = 0
        }

        switch (jumpCount) {
            case 0..JUMP_FRAME-1:
                y -= MOVE_DELTA_Y
                stretch = true
                jumpCount++
                break
            case JUMP_FRAME..JUMP_FRAME*2:
                y += MOVE_DELTA_Y
                stretch = true
                jumpCount++
                break
            default:
                stretch != stretch
                jumpCount = Integer.MIN_VALUE
                break
        }

    }

    def isGoAway() {
        return x < -width
    }

    def isJumpTop() {
        return jumpCount == JUMP_FRAME
    }

}

trait SheepFrame extends Area {

    Area fence

    Area ground

    Area sheep

    boolean sheepAppend

    int sheepCount

    def sheepFlock = []

    def SKY_RGB = [ 150, 150, 255 ] as int[]

    def GROUND_RGB = [ 100, 255, 100 ] as int[]

    def init(int width, int height, int sheepWidth, int sheepHeight, int fenceWidth, int fenceHeight) {
        x = 0
        y = 0
        this.width = width
        this.height = height

        sheep = new SimpleArea(0, 0, sheepWidth, sheepHeight)
        fence = new SimpleArea(((width - fenceWidth)  / 2) as int, height - fenceHeight, fenceWidth, fenceHeight)
        ground = new SimpleArea(0, (height - fenceHeight * 0.9) as int, width, (fenceHeight * 0.9) as int)
    }

    def run() {

        if (sheepAppend) {
            sheepFlock.add(new Sheep(sheep, this, ground, fence))
        }

        def removeFlock = new LinkedList<Sheep>()
        for (sheep in sheepFlock) {
            sheep.run()
            if (sheep.isJumpTop()) {
                sheepCount += 1
            } else if (sheep.isGoAway()) {
                removeFlock.add(sheep)
            }
        }

        sheepFlock.removeAll(removeFlock)

        if (sheepFlock.size() <= 0) {
            sheepFlock.add(new Sheep(sheep, this, ground, fence))
        }

    }

}

class SheepCanvas extends Canvas implements SheepFrame {

    Color SKY_COLOR

    Color GROUND_COLOR

    boolean running

    def sheepImage = new BufferedImage[2]

    Image fenceImage

    Image offScreenImage

    SheepCanvas(int width, int height) {
        size = new Dimension(width, height)

        SKY_COLOR = new Color(SKY_RGB[0] as int, SKY_RGB[1] as int, SKY_RGB[2] as int)
        GROUND_COLOR = new Color(GROUND_RGB[0] as int, GROUND_RGB[1] as int, GROUND_RGB[2] as int)

        fenceImage = ImageIO.read(getClass().getResourceAsStream("fence.png"))
        sheepImage[0] = ImageIO.read(getClass().getResourceAsStream("sheep00.png"))
        sheepImage[1] = ImageIO.read(getClass().getResourceAsStream("sheep01.png"))

        init(width, height, sheepImage[0].width, sheepImage[0].height, fenceImage.width, fenceImage.height)

        addMouseListener(new MouseAdapter() {
            @Override
            void mousePressed(MouseEvent e) {
                super.mousePressed(e)
                sheepAppend = true
            }
            @Override
            void mouseReleased(MouseEvent e) {
                super.mouseReleased(e)
                sheepAppend = false
            }
        })

    }

    void start() {

        if (offScreenImage == null) {
            offScreenImage = createImage(120, 120)
        }

        running = true
        (new Thread(new Runnable() {
            @Override
            void run() {
                while (running) {
                    SheepCanvas.this.run()
                    repaint()
                    Thread.sleep(100)
                }
            }
        })).start()

    }

    void stop() {

        running = false

    }

    @Override
    void update(Graphics g) {
        super.update(g)
        if (offScreenImage != null) {
            def bg = offScreenImage.graphics

            bg.color = SKY_COLOR
            bg.fillRect(x, y, width, height)

            bg.color = GROUND_COLOR
            bg.fillRect(ground.x, ground.y, ground.width, ground.height)

            bg.drawImage(fenceImage, fence.x, fence.y, fence.width, fence.height, null)

            for (Sheep sheep in sheepFlock) {
                if (sheep.stretch) {
                    bg.drawImage(sheepImage[0], sheep.x, sheep.y, sheep.width, sheep.height, null)
                } else {
                    bg.drawImage(sheepImage[1], sheep.x, sheep.y, sheep.width, sheep.height, null)
                }
            }

            bg.color = Color.BLACK
            bg.drawString(sheepCount + " woolmark", 5, 5 + bg.font.size)

            g.drawImage(offScreenImage, 0, 0, null)
        }
    }

}

SwingUtilities.invokeAndWait(new Runnable() {
    @Override
    void run() {

        println((new File("")).absolutePath)

        def canvas = new SheepCanvas(120, 120)

        def window = new JWindow()
        window.add(canvas)
        window.pack()

        window.addWindowListener(new WindowAdapter() {
            @Override
            void windowOpened(WindowEvent e) {
                canvas.start()
            }
            @Override
            void windowClosed(WindowEvent e) {
                canvas.stop()
            }
        })

        def mouseAdapter = new MouseAdapter() {
            MouseEvent start
            @Override
            void mousePressed(MouseEvent e) {
                if (e.clickCount == 2) {
                    start = e;
                } else {
                    start = null
                }
            }
            @Override
            void mouseReleased(MouseEvent e) {
                start = null
            }
            @Override
            void mouseDragged(MouseEvent e) {
                if (start != null) {
                    def win = SwingUtilities.windowForComponent(e.component)
                    if (win != null) {
                        def loc = e.locationOnScreen
                        window.setLocation((loc.x - start.x) as int, (loc.y - start.y) as int)
                    }
                }
            }
        }

        canvas.addMouseListener(mouseAdapter)
        canvas.addMouseMotionListener(mouseAdapter)

        window.toFront()
        window.locationByPlatform = true
        window.visible = true

    }
})