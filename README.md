# Sheep for Groovy

Sheep for Groovy is an implementation [sheep] for [Groovy].

# Requirements

 - Groovy 2.4+
 - JetBrains IntelliJ and Kotlin Plugin

# License
[WTFPL] 

[sheep]: https://woolmark.bitbucket.org/ "Sheep"
[Groovy]: http://groovy-lang.org "Groovy"
[WTFPL]: http://www.wtfpl.net "WTFPL"

